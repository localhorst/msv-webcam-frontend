const boxes = document.getElementsByClassName('current_webcam_image_box');
const reload_interval = setInterval(reloadImages, 60000);
const wakeup_hour = 5; //UTC
const sleep_hour = 18; //UTC

window.onload = function() {
    reloadImages();
};

function checkImageSource(image_url) {
    var http_request = new XMLHttpRequest();
    http_request.open('HEAD', image_url, false);
    http_request.send();

    return http_request.status;
}

function reloadImages() {
    console.log('reload in progress');

    Array.prototype.forEach.call(boxes, function(box) {
        var headline = box.childNodes[1]
        var image = box.childNodes[3]
        var status_text = box.childNodes[5]
        var sleep_text = box.childNodes[7]

        if (checkImageSource(image.src) == 200) {
            status_text.style = "visibility:hidden"
            sleep_text.style = "visibility:hidden"
            image.style = "width:100%;visibility:visible"
            if (!image.src.includes('?')) {
                image.src = `${image.src}?${Date.now()}`;
            } else {
                image.src = image.src.slice(0, image.src.indexOf('?') + 1) + Date.now();
            }
        } else {

            console.log('unable to find image on server');

            const now = new Date();
            const currentHour = now.getUTCHours();
            console.log(currentHour)

            if ((currentHour >= wakeup_hour) && (currentHour < sleep_hour)) {
                console.log("Camera error/fault occurred")
                sleep_text.style = "visibility:hidden"
                image.alt = "Webcam nicht erreichbar."
                image.style = "width:100%;visibility:hidden"
                status_text.style = "visibility:visible"
            } else {
                console.log("Camera in sleep mode")
                status_text.style = "visibility:hidden"
                image.alt = "Webcam im Ruhemodus."
                image.style = "width:100%;visibility:hidden"
                sleep_text.style = "visibility:visible"
            }
        }
    });
    console.log('reload finished');
}