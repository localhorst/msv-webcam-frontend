# msv-webcam-frontend

- `zypper install avif-tools`

- `mkdir /opt/msv-webcam-frontend/`
- `cd /opt/msv-webcam-frontend/`
- import `msv_webcam_frontend.py`
- `chmod +x /opt/msv-webcam-frontend/msv_webcam_frontend.py` 
- `nano /etc/systemd/system/msv-webcam-frontend.service`
- create dst dir `mkdir -p /var/www/html/msv-buehl-moos.de/webcam/data/camera/`
- `systemctl daemon-reload && systemctl enable --now msv-webcam-frontend.service`

- copy WebApp to `/var/www/html/msv-buehl-moos.de/webcam/`